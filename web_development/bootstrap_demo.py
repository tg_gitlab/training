from flask import Flask, render_template
 
app = Flask(__name__)

@app.route('/')
def homepage():

    title = "Web Development"
    paragraph = "Dit is een leuke nieuwe webapplicatie die met een paar drukken op een paar knoppen is gemaakt."

    return render_template("bs_demo.html", title=title, paragraph=paragraph)

@app.route('/about')
def aboutpage():

    title = "Over deze webapplicatie"
    paragraph = "Dit is tekst die vertelt wat deze applicatie doet."

    return render_template("bs_demo.html", title=title, paragraph=paragraph)


@app.route('/contact')
def contactPage():

    title = "Contact opnemen"
    paragraph = "Neem gerust contact met ons op! Rooksignalen, semaforen, morse code... Alles is prima!"

    return render_template("bs_demo.html", title=title, paragraph=paragraph)

 
if __name__ == "__main__":
	app.run(debug = True, host='localhost', port=5006, passthrough_errors=True)

